<?php
/**
 * @file
 * drupal_ladder_view.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function drupal_ladder_view_taxonomy_default_vocabularies() {
  return array(
    'ladder' => array(
      'name' => 'Ladder',
      'machine_name' => 'ladder',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
  );
}
