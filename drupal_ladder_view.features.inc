<?php
/**
 * @file
 * drupal_ladder_view.features.inc
 */

/**
 * Implements hook_views_api().
 */
function drupal_ladder_view_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function drupal_ladder_view_node_info() {
  $items = array(
    'person' => array(
      'name' => t('Person'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  return $items;
}
